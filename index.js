const trimWhiteSpace = (word) => word.trim();
const minWordLength = (minLength) => word => word.length > minLength;
const isNotNumber = (word) => isNaN(word);
const collectWordsByFrequency = (collection, word) => (collection[word] = (collection[word] || 0) + 1, collection);
const sortWordByFrequency = ([_, a1], [__, b1]) => b1 - a1;

const wordsByFrequency = book_text.split(' ')
    .filter(trimWhiteSpace)
    .filter(minWordLength(1))
    .filter(isNotNumber)
    .map(word => word.toLowerCase())
    .reduce(collectWordsByFrequency, {});

const top20 = Object.entries(wordsByFrequency)
    .sort(sortWordByFrequency)
    .slice(0, 20);
top20.forEach(([word, frequency]) => document.write(`${frequency} ${word} <br>`));
console.log(top20);


